<?php

namespace Tell;

use pocketmine\plugin\PluginBase;
use pocketmine\event\Listener;
use pocketmine\utils\TextFormat;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\Player;

class Main extends PluginBase implements Listener {

	public function onEnable() {
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
		$this->getLogger()->info("Enabled v1.0 by TutoGamerWalid !");
	}

	public function onCommand(CommandSender $sender, Command $command, $label, array $args) {
		$commandName = $command->getName();
		if($commandName === "chat") {
			$player = $this->getServer()->getPlayer($args[0]);
			$sender->sendMessage(TextFormat::GOLD."[TellWars] Your message has was sent to ".$args[0]." !");
 		}else {
		$this->getLogger()->info("use command in-game !");
		}
		if($player instanceof Player) {
			$name = $sender->getName();
			array_shift($args);
			$msg = implode(" ",$args);
			$player->sendMessage("[TellWars] ".$args[0]." ".$msg." ");
		}else {
			$sender->sendMessage("[TellWars] ".$args[0]." not found !");
			return true;				
		     }
	     }
     }
