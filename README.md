# TellWars by MinePM

# How does this plugin work?

This is a chat plugin (like the default /tell)

# How do I use it?

You use /chat <player> <message>

# How do I install it?

Download the phar file and drop it into your 'plugins folder'.

Remember! This is a plugin for server software for MCPE, it won't work on Bukkit, spigot etc...

# Credits

This plugin was developed by [TutoGamerWalid] (https://GitHub.com/wiligangster) and [Martin77Epic] (https://GitHub.com/Martin77Epic).

